package br.com.automation.APIRest;

import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

public class AppTest {
	
	public static String ID_DO_FILME = "tt1285016";
	public static String API_KEY = "52ec71bf";
	
	@Test
	public void Teste1ResponseOK() {
		String uriBase = "https://www.omdbapi.com/";
		given()
			.relaxedHTTPSValidation()
			.param("i", ID_DO_FILME)
			.param("apikey", API_KEY)
		.when()
			.get(uriBase)
		.then()
			.statusCode(200) // O status http retornado foi 200
			.body("Title", containsString("The Social Network"))
			.body("Released", containsString("01 Oct 2010"))
			.body("Language", containsString("English, French"));

	}
	
	@Test
	public void Teste2ResponseSemFilme() {
		ID_DO_FILME = "99999999";
		String uriBase = "https://www.omdbapi.com/";
		given()
			.relaxedHTTPSValidation()
			.param("i", ID_DO_FILME)
			.param("apikey", API_KEY)
		.when()
			.get(uriBase)
		.then()
			.statusCode(200) // O status http retornado foi 200
			.body("Response", containsString("False"))
			.body("Error", containsString("Incorrect IMDb ID"));

	}
}

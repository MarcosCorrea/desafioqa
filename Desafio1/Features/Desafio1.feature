#language: pt
Característica: Pesquisa na Unimed

#Cenário: Pesquisa com retorno válido
#	Dado que eu abro o navegador e acesso o portal
#	Quando eu acesso o menu "Guia Médico"
#	E eu faço a busca por "Médico"
#	E eu busco pelo estado "Rio de Janeiro", pela Cidade "Rio de Janeiro" e seleciono a unidade "UNIMED RIO"
#	Então eu valido que os resultados com especialidade e cidade são apresentados
	

Cenário: Pesquisa com retorno não existente
	Dado que eu abro o navegador e acesso o portal
	Quando eu acesso o menu "Guia Médico"
	E eu faço a busca por "Médico"
	E eu busco pelo estado "Rio de Janeiro", pela Cidade "Rio de Janeiro" e seleciono a unidade "UNIMED RIO"
	Então eu valido que não serão retornados resultados da cidade São Paulo
	Quando eu clico na página de pesquisa "2"
	Então eu valido que não serão retornados resultados da cidade São Paulo
	Quando eu clico na página de pesquisa "3"
	Então eu valido que não serão retornados resultados da cidade São Paulo
	

package StepDefinition;		

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;		
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;		
import cucumber.api.java.en.Then;		
import cucumber.api.java.en.When;

public class StepDesafio1 {				

    WebDriver driver;			
    		
    @Given("^que eu abro o navegador e acesso o portal$")					
    public void abrir_Chrome_e_acessar_o_portal() throws Throwable							
    {		
       System.setProperty("webdriver.chrome.driver", "/home/sympla/eclipse-workspace/CucumberWithSelenium/src/chromedriver");
       ChromeOptions options = new ChromeOptions();

       Map<String, Object> prefs = new HashMap<String, Object>();
       prefs.put("profile.managed_default_content_settings.geolocation", 2);
       options.setExperimentalOption("prefs", prefs);
       driver= new ChromeDriver(options);					
       driver.manage().window().maximize();			
       driver.get("https://www.unimed.coop.br/");					
    }		

    @When("^eu acesso o menu \"([^\"]*)\"$")			
    public void eu_acesso_o_menu(String menu) throws Throwable 							
    {		
       driver.findElement(By.xpath("//a[contains(text(), '" + menu + "')]")).click();
    }	

    @When("^eu faço a busca por \"([^\"]*)\"$")					
    public void eu_faco_a_busca_por(String pesquisa) throws Throwable 							
    {		
       driver.findElement(By.id("campo_pesquisa")).sendKeys(pesquisa);							
       driver.findElement(By.id("btn_pesquisar")).click();	
       Thread.sleep(500);
    }	
    
    @When("^eu busco pelo estado \"([^\"]*)\", pela Cidade \"([^\"]*)\" e seleciono a unidade \"([^\"]*)\"$")					
    public void eu_busco_pelo_Estado_pela_Cidade_e_seleciono_a_unidade(String estado, String cidade, String unidade) throws Throwable 							
    {		
       driver.findElement(By.xpath("//div[contains(@class, 's-field control-group selecione-rede big-field pesquisa-avancada')]")).click();
       Actions actions = new Actions(driver);
       actions.click();
       actions.sendKeys(estado);
       Thread.sleep(2000);
       actions.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
       actions.build().perform();
       driver.findElement(By.xpath("//div[contains(@class, 's-field control-group selecione-plano big-field pesquisa-avancada')]")).click();
       actions.sendKeys(cidade);
       Thread.sleep(2000);
       actions.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
       actions.build().perform();
       Thread.sleep(2000);
       driver.findElement(By.xpath("//div[contains(text(), '"+ unidade +"')]")).click();
       driver.findElement(By.xpath("//button[contains(text(), 'Continuar')]")).click();
    }	
    
    @When("^eu clico na página de pesquisa \"([^\"]*)\"$")					
    public void eu_clico_na_pagina_de_pesquisa(String pesquisa) throws Throwable 							
    {		
       driver.findElement(By.linkText(pesquisa)).click();							
       Thread.sleep(2000);
    }	

    @Then("^eu valido que os resultados com especialidade e cidade são apresentados$")					
    public void eu_valido_que_os_resultados_com_especialidade_e_cidade_sao_apresentados() throws Throwable 							
    {		
       Thread.sleep(2000);
       String caminhoesp = "(//span[@id='txt_especialidade']//following-sibling::span)[1]";
       String caminhocidade = "(//span[@id='txt_endereco']/p)[1]";
       WebDriverWait wait = new WebDriverWait(driver, 120);
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(caminhoesp)));
       Assert.assertEquals("Anestesiologia", driver.findElement(By.xpath(caminhoesp)).getText());
       Assert.assertTrue(driver.findElement(By.xpath(caminhocidade)).getText().contains("Rio de Janeiro"));
    }
    
    @Then("^eu valido que não serão retornados resultados da cidade São Paulo$")					
    public void eu_valido_que_nao_serao_retornados_resultados_da_cidade_Sao_Paulo() throws Throwable 							
    {		
       Thread.sleep(2000);
       String caminhocidade = "//span[@id='txt_endereco']/p";
       WebDriverWait wait = new WebDriverWait(driver, 120);
       wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(caminhocidade)));
       String validacao;
       
       int tamanho = driver.findElements(By.xpath("//span[@id='txt_endereco']/p")).size();
       
       for(int i=1; i<= tamanho; i++)
       {
    	   validacao = driver.findElement(By.xpath("(//span[@id='txt_endereco']/p)["+i+"]")).getText();
    	   System.out.println(validacao);
    	   if(validacao.contains("São Paulo"))
    	   {
    		   Assert.assertTrue(false);
    	   }
    	   else
    	   {
    		   Assert.assertTrue(true);   
    	   }
       }
    }
    
    

}